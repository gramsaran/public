asnp Citrix*

$ddc01 = ""
$ddc02 = "" 

#set static & email variables 
$machineid = $env:COMPUTERNAME
$body = $null
$reportdate = get-date -Format "MM-dd-yyyy HH:mm"

$messageParameters = @{
	from = "From Address"
	To = "Comma Separated List Of To Addresses 
	"
	SmtpServer = "SMTP Server"
	subject = "Citrix VDI Health Check | " + $reportdate}

#Test Internal PROD Citrix URL
if (Test-NetConnection Ip/Host name Of Internal NS -Port 443 |select -ExpandProperty TcpTestSucceeded){$ProdInternalCitrixURL = "Up"}
else {$ProdInternalCitrixURL = "DOWN"}


#Test External PROD Citrix URL
if (Test-NetConnection Ip/Host name Of External NS GATEWAY -Port 443 |select -ExpandProperty TcpTestSucceeded){$ProdExternalCitrixURL = "Up"}
else {$ProdExternalCitrixURL = "DOWN"}

#Hypervisor state
if (Get-BrokerHypervisorConnection -AdminAddress $ddc01 | select -ExpandProperty state){$HypervisorState = "Up"}
else {$HypervisorState = "DOWN"}

#Session Counts
$ApplicationSessionCount = (Get-BrokerSession -AdminAddress $ddc01 -SessionType Application -MaxRecordCount 999 ).count
$DesktopSessionCount = (Get-BrokerSession -AdminAddress $ddc01 -SessionType Desktop -MaxRecordCount 999 ).count
$TotalSessionCount = $ApplicationSessionCount + $DesktopSessionCount

#Server/Desktop Registered Counts
$RegisteredAppServers = (Get-BrokerDesktop -OSType "Windows 2012 R2"  -MaxRecordCount 999 | where {$_.RegistrationState -eq "Registered" -and $_.DesktopGroupName -notlike "*Test*"}).count
$UnRegisteredAppServers = (Get-BrokerDesktop -OSType "Windows 2012 R2"  -MaxRecordCount 999 | where {$_.RegistrationState -eq "UnRegistered" -and $_.DesktopGroupName -notlike "*Test*"}).count
$RegisteredDesktops = (Get-BrokerDesktop -OSType "Windows 10"  -MaxRecordCount 999 | where {$_.RegistrationState -eq "Registered"}).count
$UnRegisteredDesktops = (Get-BrokerDesktop -OSType "Windows 10"  -MaxRecordCount 999 | where {$_.RegistrationState -eq "UnRegistered"}).count

#DDC Services
$FailedCitrixServices = get-service Citrix* -computername $ddc01 | where {$_.status -ne "running"}|select -ExpandProperty displayname
if ($FailedCitrixServices -eq $null){$ddc01Status = "Services Healthy"
}else {$ddc01Status = "Service Unhealthy, please check " + $FailedCitrixServices}

$FailedCitrixServices = get-service Citrix* -computername $ddc02 | where {$_.status -ne "running"}|select -ExpandProperty displayname
if ($FailedCitrixServices -eq $null){$ddc02Status = "Services Healthy"
}else {$ddc02Status = "Service Unhealthy, please check " + $FailedCitrixServices}

#################################################
# Create Prod Citrix Status DataTable
#################################################

$table1 = New-Object system.Data.DataTable "Prod Citrix Services Status"
$col1 = New-Object system.Data.DataColumn URLStatus, ([string])
$col2 = New-Object system.Data.DataColumn Status, ([string])
$table1.columns.add($col1)
$table1.columns.add($col2)

#Add content to the DataTable
$row = $table1.NewRow()
$row.URLStatus = "Internal Citrix URL"
$row.Status = $ProdInternalCitrixURL
$table1.Rows.Add($row)
$row = $table1.NewRow()
$row.URLStatus = "External Citrix URL"
$row.Status = $ProdExternalCitrixURL
$table1.Rows.Add($row)
$row = $table1.NewRow()
$row.URLStatus = "Hypervisor Status"
$row.Status = $HypervisorState
$table1.Rows.Add($row)
$row = $table1.NewRow()

$ProdCitrixStatusHTML = "<table width=25%><caption>Prod Citrix URL Status</caption><tr><th>Prod Citrix URL Status</th><th>Status</th></tr>"
foreach ($row in $table1.Rows)
{
	$ProdCitrixStatusHTML += "<tr><td>" + $row[0] + "</td><td>" + $row[1] + "</td></tr>"
}
$ProdCitrixStatusHTML += "</table>"



# Create Current Session DataTable
$table2 = New-Object system.Data.DataTable "Prod Session Information"
$col1 = New-Object system.Data.DataColumn SessionInfo, ([string])
$col2 = New-Object system.Data.DataColumn Status, ([string])
$table2.columns.add($col1)
$table2.columns.add($col2)

#Add content to the DataTable
$row = $table2.NewRow()

$row.SessionInfo = "Application Session Connected"
$row.Status = $ApplicationSessionCount
$table2.Rows.Add($row)
$row = $table2.NewRow()
$row.SessionInfo = "Desktop Session Connected"
$row.Status = $DesktopSessionCount
$table2.Rows.Add($row)
$row = $table2.NewRow()
$row.SessionInfo = "Total Session Connected"
$row.Status = $TotalSessionCount
$table2.Rows.Add($row)
# Create an HTML version of the DataTable
$ProdCitrixSessionHTML = "<table width=25%><caption>Prod Session Information</caption><tr><th>VDI Services Checklist</th><th>Status</th></tr>"
foreach ($row in $table2.Rows)
{
	$ProdCitrixSessionHTML += "<tr><td>" + $row[0] + "</td><td>" + $row[1] + "</td></tr>"
}
$ProdCitrixSessionHTML += "</table>"


#################################################
#Create Desktop/App Registration DataTable
#################################################

$table3 = New-Object system.Data.DataTable "Machine Registration"
$col1 = New-Object system.Data.DataColumn DeliveryGroupType, ([string])
$col2 = New-Object system.Data.DataColumn RegisteredCounts, ([string])
$col3 = New-Object system.Data.DataColumn UnRegisteredCounts, ([string])
$table3.columns.add($col1)
$table3.columns.add($col2)
$table3.columns.add($col3)

#Add content to the DataTable
$row = $table3.NewRow()
$row.DeliveryGroupType = "Server OSs"
$row.RegisteredCounts = $RegisteredAppServers
$row.UnRegisteredCounts = $UnRegisteredAppServers
$table3.Rows.Add($row)
$row = $table3.NewRow()
$row.DeliveryGroupType = "Desktop OSs"
$row.RegisteredCounts = $RegisteredDesktops
$row.UnRegisteredCounts = $UnRegisteredDesktops
$table3.Rows.Add($row)
# Create an HTML version of the DataTable
$DeliveryGroupTypeHTML = "<table width=25%><caption>Desktop/App Registration</caption><tr><th>OS Type</th><th>Registered Machines</th><th>Unregistered Machines</th></tr>"
foreach ($row in $table3.Rows)
{
	$DeliveryGroupTypeHTML += "<tr><td>" + $row[0] + "</td><td>" + $row[1] + "</td><td>" + $row[2] + "</td></tr>"
}
$DeliveryGroupTypeHTML += "</table>"

#################################################
###   Server Health Status DataTable
#################################################
$table4 = New-Object system.Data.DataTable "Server Health Status"
$col1 = New-Object system.Data.DataColumn ServerName, ([string])
$col2 = New-Object system.Data.DataColumn Status, ([string])
$table4.columns.add($col1)
$table4.columns.add($col2)

#Add content to the DataTable
$row = $table4.NewRow()
$row.ServerName = $ddc01
$row.Status = $ddc01Status
$table4.Rows.Add($row)
$row = $table4.NewRow()
$row.ServerName = $ddc02
$row.Status = $ddc02Status
$table4.Rows.Add($row)
# Create an HTML version of the DataTable
$ServerHealthStatusHTML = "<table width=25%><caption>Server Health Status</caption><tr><th>Server Name</th><th>Status</th></tr>"
foreach ($row in $table4.Rows)
{
	$ServerHealthStatusHTML  += "<tr><td>" + $row[0] + "</td><td>" + $row[1] + "</td></tr>"
}
$ServerHealthStatusHTML  += "</table>"

#email me!
$row = $null

$body = "<HTML><HEAD><META http-equiv=""Content-Type"" content=""text/html; charset=iso-8859-1"" /><style>table, th, td {border: 1px solid black;}</style><TITLE></TITLE></HEAD>"
$body += "</br>"
$body += $ProdCitrixStatusHTML
$body += "</br>"
$body += $ProdCitrixSessionHTML
$body += "</br>"
$body += $DeliveryGroupTypeHTML
$body += "</br>"
$body += $ServerHealthStatusHTML 
$body += "</br>"
$body += "Stats for Nerds: Run from <b>$machineid </b> | Date <b>$reportdate</b>"

Send-MailMessage @messageParameters -Body $body -BodyAsHtml
