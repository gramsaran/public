#UPDATEALLTHEAPPS!!!
$architecture = "x64"
$msi = "msi"

$downloadpath = "\\ctxfileprod\file\Apps\3rd Party Patches"

#NotePad++
Get-EvergreenApp notepadplusplus |where {$_.architecture -eq $architecture  -and $_.type -eq "exe"} | Save-EvergreenApp -CustomPath $downloadpath  -force

#Chrome
Get-EvergreenApp googlechrome |where {$_.architecture -eq $architecture  -and $_.Type -eq $msi  -and $_.Channel -eq "Stable"} | Save-EvergreenApp -CustomPath $downloadpath -force

#Edge
Get-EvergreenApp microsoftedge |where {$_.architecture -eq $architecture  -and $_.release -eq "enterprise" -and $_.channel -eq "stable"} | Save-EvergreenApp -CustomPath $downloadpath -force

#Edge View 2
Get-EvergreenApp MicrosoftEdgeWebView2Runtime |where {$_.architecture -eq $architecture } |Save-EvergreenApp -CustomPath $downloadpath -force

#firefox 
Get-EvergreenApp mozillafirefox |  where {$_.architecture -eq $architecture  -and $_.Type -eq $msi  -and $_.Channel -eq "LATEST_FIREFOX_VERSION"} | Save-EvergreenApp -CustomPath $downloadpath -force

#WinSCP
Get-EvergreenApp wireshark |where {$_.Version -eq "4.2.2" -and $_.Architecture -eq $architecture -and $_.type -eq $msi }  | Save-EvergreenApp -CustomPath $downloadpath -force
